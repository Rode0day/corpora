.PHONY: all tcpdump clean install

VERSION = tcpdump-4.9.3
PCAPVER = libpcap-1.9.1
BINARY = tcpdump
LIBPCAP = $(PCAPVER)/libpcap.a
PREFIX ?= verify

all: tcpdump

tcpdump:
	make -C src

clean:
	rm -rf lava-install
	make -C src clean
	rm -rf $(VERSION)
	rm -rf $(PCAPVER)
	rm -rf $(PREFIX)

install:
	rm -rf lava-install
	mkdir -p lava-install/bin
	cp src/tcpdump lava-install/bin

$(VERSION):
	wget -qO- https://github.com/the-tcpdump-group/tcpdump/archive/$(VERSION).tar.gz | tar xz && \
	mv tcpdump-$(VERSION) $(VERSION)

$(PCAPVER):
	wget -qO- https://github.com/the-tcpdump-group/libpcap/archive/$(PCAPVER).tar.gz | tar xz && \
	mv libpcap-$(PCAPVER) $(PCAPVER)

$(LIBPCAP): $(PCAPVER)
	cd $(PCAPVER); \
	./configure CFLAGS="${CFLAGS}" --disable-shared >/dev/null && \
	make -j >/dev/null

$(VERSION)/$(BINARY): $(VERSION) $(LIBPCAP)
	cd $(VERSION); \
	./configure > /dev/null && \
	make -j >/dev/null

$(PREFIX): $(VERSION)/$(BINARY)
	rm -rf $(PREFIX)
	mkdir -p $(PREFIX)/bin
	cp $< $(PREFIX)/bin/
