#!/bin/bash

export CC=${CC:-gcc}
export CFLAGS=${CFLAGS:--g -w -O0}

declare -A CC_ABV
CC_ABV[afl-clang-fast]="afl-cf"
CC_ABV[hfuzz-clang]="hf"
CC_ABV[angora-clang]="ang"
CC_ABV[gclang]="bc"

LAVA_M="lava_corpus/LAVA-M"
BN=${CC##*/}
DEFAULT_PREFIX="lava-${CC_ABV[$BN]:-$BN}"
PREFIX="${PREFIX:-$DEFAULT_PREFIX}"

[ -d 0 ] && cd 0

if [ ! -e lava_corpus.patch ]; then
    echo "[-] Error: could not find patch file 'lava_corpus.patch'."
    exit 1
fi

echo "[*] Downloading lava_corpus.tar.xz"
wget -qO- http://moyix.net/~moyix/lava_corpus.tar.xz | tar -xJf -

if [ ! -d lava_corpus ]; then
    echo "[-] Error: failed to download lava_corpus!"
    exit 1
fi

echo "[*] Applying patch file"
# git -C lava_corpus apply ../lava_corpus.patch
patch -d lava_corpus -p1 -i ../lava_corpus.patch


patch_uniq_for_angora() {
    # fix according to (https://github.com/AngoraFuzzer/Angora/blob/master/docs/lava.md)
    find . -type f -name "*.h" -exec sed -i 's/#define\s*HAVE_GETC_UNLOCKED\s*[0-9]/#undef HAVE_GETC_UNLOCKED/' {} +
    find . -type f -name "*.h" -exec sed -i 's/#define\s*HAVE_DECL_GETC_UNLOCKED\s*[0-9]/#undef HAVE_GETC_UNLOCKED/' {} +
}

validate() {
    cd coreutils-8.24-lava-safe
    make clean &> /dev/null
    ./configure --prefix=`pwd`/lava-install --disable-acl --disable-xattr --without-selinux --disable-shared &> /dev/null
    if [[ ( $CC == *angora-clang || $CC == *glcang ) && $target == uniq ]]; then
        patch_uniq_for_angora
    fi
    make -j $(nproc) &> /dev/null
}

build_lava_angora_track() {
    local RET=0
    echo "[*] Building angora taint tracking binary ${target}.tt"
    pushd ${LAVA_M}/${target}/coreutils-8.24-lava-safe >/dev/null
    make clean &>/dev/null
    USE_TRACK=1 CC=/angora/bin/angora-clang CFLAGS="$CFLAGS" make -j || RET=1
    popd >/dev/null
    cp ${LAVA_M}/${target}/coreutils-8.24-lava-safe/src/${target} \
        ${target}/${PREFIX}/bin/${target}.tt
    return $RET
}


build_lava() {
    local target=${1:-base64}
    pushd ${LAVA_M}/${target} >/dev/null
    validate
    popd >/dev/null

    mkdir -p ${target}/${PREFIX}/bin
    mkdir -p ${target}/inputs

    if [[ $CC == *angora-clang && $USE_TRACK == 1 ]]; then
        cp ${LAVA_M}/${target}/coreutils-8.24-lava-safe/src/${target} \
            ${target}/${PREFIX}/bin/${target}.tt
    else
        cp ${LAVA_M}/${target}/coreutils-8.24-lava-safe/src/${target} \
            ${target}/${PREFIX}/bin/
        cp ${LAVA_M}/${target}/fuzzer_input/* ${target}/inputs/
        if [[ $CC == *gclang ]]; then
            get-bc ${target}/${PREFIX}/bin/*
        fi
    fi
}


for target in base64 md5sum uniq who; do
    build_lava $target
done

rm -rf lava_corpus
