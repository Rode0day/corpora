
CC ?= clang-9
CXX ?= clang++-9
OPT ?= opt-9

CFLAGS += -w
VERIFY_CFLAGS ?= -fsanitize=address -g -w
PREFIX ?= verify

BROKEN_TARGETS = \
    8/jqB2/src/src/jq \

TARGETS = \
    2/yamlB2/src/src/deconstruct \
    4/newgrepS/src/src/newgrep \
    4/pcreB/src/src/pcre2grep \
    5/duktape/src/src/duk \
    6/jqB/src/src/jq \
    7/fileB3/src/src/file \
    9/sqliteB/src/src/sqlite \
    10/tcpdumpB/src/src/tcpdump \
    11/jpegS3/src/src/jpeg \
    11/jqS3/src/src/jq \
    11/tinyexprB2/src/src/math \
    12/fileS3/src/src/file \
    12/grepB2/src/src/grep \
    12/jqS4/src/src/jq \
    13/fileS4/src/src/file

PRED_TARGETS = $(addsuffix .pt, $(TARGETS))
BC_TARGETS = $(addsuffix .bc, $(TARGETS))

PRED_INSTALL_TARGETS = $(subst src/src,lava-install/bin, $(TARGETS))
PREFIX_INSTALL_TARGETS = $(subst src/src,$(PREFIX)/bin, $(TARGETS))

PARMESAN_TARGETS = $(addsuffix .track, $(PREFIX_INSTALL_TARGETS))
ANGORA_TARGETS = $(addsuffix .tt, $(PREFIX_INSTALL_TARGETS))

.PHONY: clean verify-clean verify-clean-all fix_jq_glibc

%.pt:
	@echo $@
	make -C $(dir $@) -j \
		CC=$(CC) \
		OPT=$(OPT) \
		PREDICATE_TRACE_PLUGIN_PATH=/usr/local/lib/libpredicate_trace_pass.so \
		PREDICATE_TRACE_SUPPORT_PATH=/usr/local/lib $(notdir $@)

%.bc :
	@echo "[*] make -C $(dir$@)"
	CFLAGS="$(CFLAGS)" make -C $(dir $@) -j CC=$(CC) 2>/dev/null
	get-bc $(basename $@)

%.track:
	@echo $@
	rm -rf seeds
	/parmesan/tools/compile_bc.sh $(basename $@).bc

%.tt:
	@echo $@
	USE_FAST=1 /angora/bin/angora-clang -o $(basename $@) $(basename $@).bc
	USE_TRACK=1 /angora/bin/angora-clang -o $@ $(basename $@).bc

predicate_trace : $(PRED_TARGETS)

bitcode : $(BC_TARGETS)

parmesan : $(PARMESAN_TARGETS)

angora : $(ANGORA_TARGETS)

install_predicate : $(PRED_TARGETS)
	$(foreach target, $(PRED_TARGETS), \
	  mkdir -p $(subst src/src,lava-install/bin, $(dir $(target))) && \
	  cp -f $(target) $(subst src/src,lava-install/bin, $(target)); )

install_prefix : $(TARGETS)
	$(foreach target, $(TARGETS), \
	  mkdir -p $(subst src/src,$(PREFIX)/bin, $(dir $(target))) && \
	  cp -f $(target) $(dir $(subst src/src,$(PREFIX)/bin, $(target))); )

install_bitcode : $(BC_TARGETS)
	$(foreach target, $(BC_TARGETS), \
	  mkdir -p $(subst src/src,$(PREFIX)/bin, $(dir $(target))) && \
	  cp -f $(target) $(dir $(subst src/src,$(PREFIX)/bin, $(target))); )


clean:
	$(foreach target, $(TARGETS), make -C $(dir $(target)) clean;)

fix_jq_glibc:
	sed -i '/f_pow10/d' */jq*/src/src/builtin-pre.c
	sed -i '/f_exp10/d' */jq*/src/src/builtin-pre.c

# This section supports building targets from clean source

SRC_TARGETS = \
    2/yamlB2/src \
    4/pcreB/src \
    5/duktape/src \
    6/jqB/src \
    7/fileB3/src \
    9/sqliteB/src \
    10/tcpdumpB/src \
    11/jpegS3/src \
    11/tinyexprB2/src \
    12/grepB2/src

SRC_BINS = \
    /bin/deconstruct \
    /bin/pcre2grep \
    /bin/duk \
    /bin/jq \
    /bin/file \
    /bin/sqlite \
    /bin/tcpdump \
    /bin/jpeg \
    /bin/math \
    /bin/grep


VERIFY_SRC_BINS = $(addprefix /$(PREFIX),$(SRC_BINS))
PRE_VERIFY_TARGETS = $(join $(SRC_TARGETS), $(VERIFY_SRC_BINS))

# Include versioned targets
ifeq ($(BUILD_VERSION),original)
	include scripts/clean_original_targets.mk
else
	include scripts/clean_current_targets.mk
endif

# link a the compiled binary into each respective target
2/yamlB2/$(PREFIX)/bin/deconstruct: 2/yamlB2/src/$(PREFIX)/bin/deconstruct
	mkdir -p $(dir $@)
	ln $< $@

4/newgrepS/$(PREFIX)/bin/newgrep: 12/grepB2/src/$(PREFIX)/bin/grep
	mkdir -p $(dir $@)
	ln $< $@

12/grepB2/$(PREFIX)/bin/grep: 12/grepB2/src/$(PREFIX)/bin/grep
	mkdir -p $(dir $@)
	ln $< $@

5/duktape/$(PREFIX)/bin/duk: 5/duktape/src/$(PREFIX)/bin/duk
	mkdir -p $(dir $@)
	ln $< $@

JQ_TARGETS = 6/jqB/$(PREFIX)/bin/jq 8/jqB2/$(PREFIX)/bin/jq 11/jqS3/$(PREFIX)/bin/jq 12/jqS4/$(PREFIX)/bin/jq

$(JQ_TARGETS) : 6/jqB/src/$(PREFIX)/bin/jq
	mkdir -p $(dir $@)
	ln $< $@

FILE_TARGETS = 7/fileB3/$(PREFIX)/bin/file 12/fileS3/$(PREFIX)/bin/file 13/fileS4/$(PREFIX)/bin/file

$(FILE_TARGETS) : 7/fileB3/src/$(PREFIX)/bin/file
	mkdir -p $(dir $@)
	ln $< $@

9/sqliteB/$(PREFIX)/bin/sqlite: 9/sqliteB/src/$(PREFIX)/bin/sqlite
	mkdir -p $(dir $@)
	ln $< $@

10/tcpdumpB/$(PREFIX)/bin/tcpdump: 10/tcpdumpB/src/$(PREFIX)/bin/tcpdump
	mkdir -p $(dir $@)
	ln $< $@

11/jpegS3/$(PREFIX)/bin/jpeg: 11/jpegS3/src/$(PREFIX)/bin/jpeg
	mkdir -p $(dir $@)
	ln $< $@

11/tinyexprB2/$(PREFIX)/bin/math: 11/tinyexprB2/src/$(PREFIX)/bin/math
	mkdir -p $(dir $@)
	ln $< $@

4/pcreB/$(PREFIX)/bin/pcre2grep: 4/pcreB/src/$(PREFIX)/bin/pcre2grep
	mkdir -p $(dir $@)
	ln $< $@

LINK_VERIFY_TARGETS = $(subst src/src,$(PREFIX)/bin, $(TARGETS))

# compile one copy of each program 
pre-verify : $(PRE_VERIFY_TARGETS)

# link compiled binaries to <install_dir>/verify/bin/<binary>
link-verify: $(LINK_VERIFY_TARGETS)

verify-bitcode :
	$(foreach target, $(LINK_VERIFY_TARGETS), get-bc $(target) ; )

src-verify :
	$(foreach target, $(SRC_TARGETS), make -C $(target) -j $(PREFIX); )

verify-clean :
	$(foreach target, $(SRC_TARGETS), make -C $(target) clean; )

clean-all : clean
	rm -rf */*/$(PREFIX)

print-verify :
	@$(foreach target, $(LINK_VERIFY_TARGETS), echo $(target);)
