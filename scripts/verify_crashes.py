#!/usr/bin/env python3

import os
import argparse
import requests
import sys
import yaml
from utils import VerifyWorker

# import credentials
from creds import HDR, base_url


def parse_info(yaml_file):
    if not os.path.isfile(yaml_file):
        raise RuntimeError("Missing info.yaml file: {}".format(yaml_file))

    info = yaml.load(open(yaml_file), Loader=yaml.SafeLoader)
    return info


def main():
    parser = argparse.ArgumentParser(description='verify Rode0day crashes')
    parser.add_argument('-c', '--challenge', default=None,
                        help='challenge name/ install_dir')
    parser.add_argument('-p', '--prefix', default='verify',
                        help='install directory prefix')
    parser.add_argument('-V', '--verified', default=None,
                        help="download crashes where verified == <verified>")
    parser.add_argument('--bug-id', default=False, action='store_true',
                        help='download crashes where bug_id is NULL')
    parser.add_argument('--coverage', default=False, action='store_true',
                        help='download crashes where bb_count is NULL')
    parser.add_argument('--continous', default=False, action='store_true',
                        help='Process crashes continously')
    parser.add_argument('-l', '--limit', default=10, type=int,
                        help='Number of crashes to process at once')
    parser.add_argument('--download-crash', default=None, metavar='crash_id', dest='crash_id',
                        help='download a specific crash sample')
    parser.add_argument('info_yaml', nargs='?', default=None, help='file path for info.yaml file')
    args = parser.parse_args()

    if args.crash_id:
        download_url = "{}/api/crash/download/{}".format(base_url, args.crash_id)
        r = requests.get(download_url, headers=HDR)
        crash_path = 'crash_id_{}'.format(args.crash_id)
        with open(crash_path, 'wb') as crash_file:
            crash_file.write(r.content)
            print("[*] Downloaded crashing input: {} ".format(crash_path))
            return

    while True:
        yaml_path = args.info_yaml
        if not os.path.isfile(yaml_path):
            print("ERROR: {} not found".format(yaml_path))
            sys.exit(1)

        info = parse_info(yaml_path)
        if args.challenge not in info['challenges']:
            print("ERROR: {} not a valid challenge name".format(args.challenge))
            sys.exit(1)

        challenge = info['challenges'][args.challenge]

        base_dir = str(info['rode0day_id'])

        online_options = {
            'base_url': base_url,
            'auth_header': HDR,
            'null_bug_id': args.bug_id,
            'null_coverage': args.coverage,
            'verified': args.verified
             }

        vw = VerifyWorker(challenge, base_dir, args.prefix, online=online_options)
        processed = vw.process_crashes(limit=args.limit)

        if not args.continous or processed == 0:
            return


if __name__ == "__main__":
    main()
    sys.exit(0)
