#!/usr/bin/env python3

import argparse
import json
import logging
import os
import sys
import yaml
from utils import VerifyWorker


logging.basicConfig(format='%(levelname)s:\t%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def parse_info(yaml_file):
    """
    Parse info.yaml for the current competition, return parsed yaml object
    """
    if not os.path.isfile(yaml_file):
        raise RuntimeError("Missing info.yaml file: {}".format(yaml_file))

    info = yaml.load(open(yaml_file), Loader=yaml.SafeLoader)
    return info


def print_results(results, seed_results=False):
    validated = len([1 for r in results if abs(r['returncode']) > 2])
    exited = len([1 for r in results if r['returncode'] == 0])
    total = len(results)

    if seed_results:
        pct_valid = float(exited / total * 100.0) if total > 0 else 0.0
        logger.info("DONE: %d of %d inputs ran cleanly. (%0.1f%%) valid ", exited, total, pct_valid)
    else:
        pct_valid = float(validated / total * 100.0) if total > 0 else 0.0
        r = [validated, validated, total, pct_valid]
        logger.info("DONE: %d bugs validated.\n\tBug coverage: %d / %d (%0.1f%%) valid ", *r)


def run_tests(info, challenge_name=None, log_results=True):
    results = list()

    if challenge_name is not None:
        if challenge_name not in info['challenges']:
            logger.error("Challenge name '%s' not found!", challenge_name)
            return
        challenge = info['challenges'][challenge_name]
        r = run_test(info, challenge)
        return r

    html_path = "results_{:02d}_{}.html".format(int(info['rode0day_id']), info['prefix'])
    log = open(html_path, 'w+') if log_results else None

    for challenge_name, challenge in info['challenges'].items():
        rtemp = run_test(info, challenge)
        results.extend(rtemp)

        if not log_results or len(rtemp) == 0:
            # skip logging results to file
            continue

        validated = len([1 for r in rtemp if abs(r['returncode']) > 2])
        total = len(rtemp)
        ratio = float(validated / total * 100.0) if validated > 0 else 0.0
        log.write("\t<tr><td>{}</td><td>{} - {}</td><td>{}</td><td>{}</td><td>{:0.1f}%</td></tr>\n".format(
                  info['prefix'],
                  info['rode0day_id'],
                  challenge_name,
                  validated,
                  total,
                  ratio))
    if log:
        log.close()
    return results


def run_test(info, challenge):
    results = list()
    base_dir = str(info['rode0day_id'])
    vw = VerifyWorker(challenge, base_dir, prefix=info['prefix'],  suffix=info['suffix'])
    r = vw.run_binary()
    if r is None:
        return results

    solutions_dir = os.path.join(base_dir, 'solutions', str(challenge['challenge_id']))
    if not os.path.isdir(solutions_dir):
        solutions_dir = os.path.join(base_dir, 'solutions', str(challenge['install_dir']))
    if not os.path.isdir(solutions_dir):
        return [r]
    for solution in os.listdir(solutions_dir):
        sol_file = os.path.join(solutions_dir, solution)
        if info.get('dynamorio_client'):
            r = vw.run_with_dynamorio_custom(sol_file, info['dynamorio_client'])
        else:
            r = vw.run_binary(sol_file)
        results.append(r)
    return results


def main():
    parser = argparse.ArgumentParser(description='test Rode0day binaries on inputs')
    parser.add_argument('-c', '--challenge', default=None,
                        help='challenge name/ install_dir')
    parser.add_argument('-p', '--prefix', default='lava-install',
                        help='install directory prefix')
    parser.add_argument('-s', '--suffix', default=None,
                        help='optional suffix for binary')
    parser.add_argument('--seeds', default=False, action='store_true',
                        help='log the results from seeds tests, not solutions')
    parser.add_argument('--dump-json', default=None,
                        help='dump logs from execution to file')
    parser.add_argument('--dynamorio-client', default=None,
                        help='dynamorio client library file')
    parser.add_argument('info_yaml', nargs='+', help='file path(s) for info.yaml file')
    args = parser.parse_args()

    results = list()
    log_results = args.dump_json is None
    for yaml_file in args.info_yaml:
        info = parse_info(yaml_file)
        info['prefix'] = args.prefix
        info['suffix'] = args.suffix
        info['dynamorio_client'] = args.dynamorio_client
        r = run_tests(info, args.challenge, log_results)
        if r:
            results.extend(r)

    if log_results:
        print_results(results, args.seeds)

    if args.dump_json:
        json.dump(results, open(args.dump_json, 'w'))


if __name__ == "__main__":
    main()
    sys.exit(0)
