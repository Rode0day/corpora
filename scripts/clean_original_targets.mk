# Clean original version builds
2/yamlB2/src/$(PREFIX)/bin/deconstruct:
	CC=${CC} CFLAGS="$(VERIFY_CFLAGS)" make -C 2/yamlB2/src -j $(PREFIX) VERSION=libyaml-0.1.7

4/pcreB/src/$(PREFIX)/bin/pcre2grep:
	CC=${CC} CFLAGS="$(VERIFY_CFLAGS)" make -C 4/pcreB/src -j $(PREFIX) VERSION=pcre2-10.33

5/duktape/src/$(PREFIX)/bin/duk:
	CC=${CC} CFLAGS="$(VERIFY_CFLAGS)" make -C 5/duktape/src -j $(PREFIX) VERSION=duktape-2.3.0

6/jqB/src/$(PREFIX)/bin/jq:
	CC=${CC} CFLAGS="$(VERIFY_CFLAGS)" make -C 6/jqB/src -j $(PREFIX) VERSION=jq-1.6

7/fileB3/src/$(PREFIX)/bin/file:
	CC=${CC} CFLAGS="$(VERIFY_CFLAGS)" make -C 7/fileB3/src -j $(PREFIX) VERSION=file-FILE5_35

9/sqliteB/src/$(PREFIX)/bin/sqlite:
	CC=${CC} CFLAGS="$(VERIFY_CFLAGS)" make -C 9/sqliteB/src -j $(PREFIX) VERSION=sqlite-autoconf-3290000 SRC_YEAR=2019

10/tcpdumpB/src/$(PREFIX)/bin/tcpdump:
	CC=${CC} CFLAGS="$(VERIFY_CFLAGS)" make -C 10/tcpdumpB/src -j $(PREFIX) VERSION=tcpdump-4.9.2 PCAPVER=libpcap-1.9.0

11/jpegS3/src/$(PREFIX)/bin/jpeg:
	CC=${CC} CFLAGS="$(VERIFY_CFLAGS)" make -C 11/jpegS3/src -j $(PREFIX) VERSION=jpeg-9c

11/tinyexprB2/src/$(PREFIX)/bin/math:
	CC=${CC} CFLAGS="$(VERIFY_CFLAGS)" make -C 11/tinyexprB2/src -j $(PREFIX)

12/grepB2/src/$(PREFIX)/bin/grep:
	CC=${CC} CFLAGS="$(VERIFY_CFLAGS)" make -C 12/grepB2/src -j $(PREFIX) VERSION=grep-3.1
