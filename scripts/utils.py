import glob
import hashlib
import json
import logging
import os
import requests
import resource
import shlex
import sys
import tempfile
from distutils.dir_util import copy_tree
from subprocess import run, Popen, PIPE, DEVNULL, STDOUT, TimeoutExpired

import drcov
try:
    import exploitable
except ImportError:
    pass

DRRUN = '/opt/dr/bin64/drrun'
DRRUN32 = '/opt/dr/bin32/drrun'
DR2LCOV = '/opt/dr/tools/bin64/drcov2lcov'
DR2LCOV32 = '/opt/dr/tools/bin32/drcov2lcov'

logging.basicConfig(format='%(levelname)s:\t%(message)s')
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

exploitable_levels = {
    'EXPLOITABLE': 9,
    'PROBABLY_EXPLOITABLE': 7,
    'PROBABLY_NOT_EXPLOITABLE': 4,
    'UNKNOWN': 3,
    }


def safe_json(text):
    try:
        j = json.loads(text)
    except ValueError:
        print("Invalid response from server:\n\t{}".format(text))
        sys.exit(1)
    return j


def sha1sum(file_path):
    with open(file_path, 'rb') as f:
        m = hashlib.sha1()
        m.update(f.read())
        return m.hexdigest()


# stolen from https://github.com/ForAllSecure/bncov
def is64bit(binary_filepath):
    with open(binary_filepath, "rb") as f:
        data = f.read(32)
    e_machine = data[18]
    if isinstance(e_machine, str):  # python2/3 compatibility, handle e_machine as string or value
        e_machine = ord(e_machine)
    if e_machine == 0x03:  # x86
        return False
    elif e_machine == 0x3e:  # x64
        return True
    raise Exception("[!] Unexpected value in 64-bit check: %s (e_machine: 0x%x)" % (binary_filepath, e_machine))


def parse_lava_log(log, match_id=None):
    r = {'bug_id': None, 'src_line': ""}
    lava = log.find(b'LAVALOG:')
    msg = log[lava:].split(maxsplit=3)
    r['bug_id'] = msg[1].strip()[:-1].decode('utf-8')
    r['src_line'] = msg[2].split(b'\n')[0].strip().decode('utf-8')
    if match_id:
        r['match'] = (r['bug_id'] == match_id)
    return r


def set_mem_limit(limit=1):
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    memlimit = 2**30 * limit  # 1 GB
    resource.setrlimit(resource.RLIMIT_AS, (memlimit, memlimit))


class VerifyWorker(object):

    def __init__(self, challenge, base_dir, prefix='lava-install', online=None, suffix=None):
        self.challenge = challenge
        self.install_dir = challenge['install_dir']
        self.binary_path = challenge['binary_path']
        self.challenge_id = challenge['challenge_id']
        self.base_dir = base_dir
        self.prefix = prefix
        self.suffix = suffix
        self.use_stdin = '{input_file}' not in challenge['binary_arguments']

        source_dir = os.path.join(base_dir, self.install_dir, 'src', 'src')
        self.source_dir = source_dir if os.path.isdir(source_dir) else None

        if online:
            self.base_url = online['base_url']
            self.auth_header = online['auth_header']
            self.filter_options = online

    def get_target_command(self, input_file, lavalog=False):
        lava_dir = os.path.join(self.base_dir, self.install_dir, self.prefix)
        local_dir = os.path.join(self.base_dir, self.install_dir)
        if lavalog and os.path.isdir(lava_dir):
            binary = os.path.join(lava_dir, self.binary_path.replace('built/', ''))
        else:
            binary = os.path.join(local_dir, self.binary_path)
        if self.suffix:
            binary = "{}{}".format(binary, self.suffix)
        if not os.path.isfile(binary):
            logger.info("Skipping: %s; did not find binary.", binary)
            return None
        library_dir = None
        if "library_dir" in self.challenge.keys():
            library_dir = os.path.join(local_dir, self.challenge["library_dir"])
        args = self.challenge["binary_arguments"].format(input_file=input_file, install_dir=local_dir)
        if library_dir:
            command = "LD_LIBRARY_PATH={} {} {}".format(library_dir, binary, args)
        else:
            command = "{binary} {args}".format(binary=binary, args=args)
        return command

    def get_coverage_stats(self, cov, module_name):
        trace = cov.get_offsets(module_name)
        return {'bb_all': cov.bb_table_count,
                'bb_ma': len(trace),
                'bb_mu': len(set(trace))}

    def add_san_options(self, san_env, values):
        for options in ['ASAN', 'UBSAN', 'MSAN', 'LSAN']:
            san_env['{}_OPTIONS'.format(options)] = "abort_on_error={}:symbolize={}".format(*values)
        return san_env

    def run_with_dynamorio(self, crash, input_file='.current_input', save_logs=False):
        """
        Run the program with Dynamorio's drcov tool and capture basic block coverage.
           If the  source directory is present, assume the binary has symbols (non stripped),
           and collect an lcov 'coverage.info' file for source line coverage.
        """

        tgt_cmd = self.get_target_command(input_file, lavalog=self.source_dir is not None)
        tgt_cmd = shlex.split(tgt_cmd)

        is64 = is64bit(tgt_cmd[0])
        drrun = DRRUN if is64 else DRRUN32
        dr2lcov = DR2LCOV if is64 else DR2LCOV32
        stdin_fd = open(input_file, 'rb') if self.use_stdin else DEVNULL
        with tempfile.TemporaryDirectory() as out:
            args = [drrun, '-t', 'drcov', '-logdir', out, '--']+tgt_cmd
#           logger.info("%s", " ".join(args))
            try:
                run(args, stdin=stdin_fd, stdout=DEVNULL, stderr=DEVNULL, timeout=30)
            except TimeoutExpired:
                logger.error("Timeout in drrun")
            finally:
                if self.use_stdin:
                    stdin_fd.close()
            files = glob.glob("{}/*.log".format(out))
            cov = drcov.DrcovData(files[0]) if files else None
            if cov is None:
                logger.error("ERROR: failed to get coverage")
                return None
            binary = os.path.split(self.binary_path)[-1]
            stats = self.get_coverage_stats(cov, binary)

            if self.source_dir and save_logs:
                cov_file = os.path.join(out, 'coverage.info')
                args = [dr2lcov, '-dir', out, '-output', cov_file, '-mod_filter', binary]
#               logger.info("%s", ' '.join(args))
                run(args, stdout=DEVNULL, stderr=DEVNULL)
            if save_logs:
                crash_dir = os.path.join('saved', self.install_dir, crash['crash_hash'])
                log_dir = os.path.join(crash_dir, 'cov')
                stats_filepath = os.path.join(crash_dir, 'crash_info.json')
                copy_tree(out, log_dir)
                with open(stats_filepath, 'w') as stats_file:
                    json.dump(crash, stats_file)

        return stats

    def run_exploitable(self, crash):
        """
        Run the program in gdb with the exploitable  GDB plugin (https://gitlab.com/wideglide/exploitable)
          originally from  Jonathan Fooote -- https://github.com/jfoote/exploitable
        """

        gdb_exploitable_path = os.path.join(exploitable.__path__[0], "exploitable.py")
        tgt_cmd = self.get_target_command('.current_input', lavalog=True)
        tgt_cmd, gdb_tgt_cmd = tgt_cmd.split(' ', 1)

        script = "source {}\nfile {}\n run {}\n exploitable -m \nquit\n".format(
            gdb_exploitable_path,
            tgt_cmd,
            gdb_tgt_cmd)
        with open('.gdb_script', 'w') as f:
            f.write(script)

        gdb_cmd = ["gdb", "-q", "--batch", "-x", ".gdb_script"]
        data = None
        asan_env = os.environ.copy()
        self.add_san_options(asan_env, (1, 0))

        stdin_fd = open('.current_input', 'rb') if self.use_stdin else DEVNULL
        try:
            p = run(gdb_cmd, stdin=stdin_fd, stdout=PIPE, stderr=STDOUT,
                    env=asan_env, timeout=10)
        except TimeoutExpired:
            logger.error("Timeout running exploitable!")
            return None
        finally:
            if self.use_stdin:
                stdin_fd.close()
        for line in p.stdout.splitlines():
            if b"classification" in line:
                data = line.decode(encoding="utf-8", errors="ingore")
            if b'LAVALOG:' in line:
                crash.update(parse_lava_log(line))
        if data is None:
            return None

        jdata = json.loads(data)
        crash.update(jdata)
        return jdata

    def run_binary(self, input_file=None, verbose=True):
        """
        Run the program on the sample input - Just useful to make sure everything is working
          (note programs may have no output with sample inputs)
        """

        using_seed = False
        r = {'returncode': 0, 'bug_id': None, 'src_line': "", 'match': False}
        local_dir = os.path.join(self.base_dir, self.challenge["install_dir"])
        if input_file is None:
            using_seed = True
            input_file = os.path.join(local_dir, self.challenge["sample_inputs"][0])
        tgt_cmd = self.get_target_command(input_file, lavalog=True)
        if tgt_cmd is None:
            return None

        stdin_fd = open(input_file, 'rb') if self.use_stdin else DEVNULL

        asan_env = os.environ.copy()
        self.add_san_options(asan_env, (1, 0))

        p = Popen(tgt_cmd, stdin=stdin_fd, stdout=PIPE, stderr=PIPE, env=asan_env, shell=True)
        try:
            sout, serr = p.communicate(timeout=15)
        except TimeoutExpired:
            logger.error("timeout expired for: %s", input_file)
            p.kill()
            return r
        finally:
            if self.use_stdin:
                stdin_fd.close()

        r['returncode'] = p.returncode
        logger.info("Executed: %s %s => %d", tgt_cmd, input_file if self.use_stdin else "", p.returncode)

        lava = serr.find(b'LAVALOG:')
        if lava > 0:
            match_id = os.path.basename(input_file).split('.')[0]
            r.update(parse_lava_log(serr, match_id))
            if not r['match'] or abs(p.returncode) < 3:
                logger.info("BUG: %s\t SRC: %s, RET: %d", r['bug_id'], r['src_line'], p.returncode)
        elif verbose and ((using_seed and p.returncode != 0) or (not using_seed and abs(p.returncode) < 3)):
            logger.error("RETURN CODE: %d\n\tSTDOUT: %s\n\tSTDERR: %s\n",
                         p.returncode,
                         sout.decode('utf-8', 'backslashreplace')[:64],
                         serr.decode('utf-8', 'backslashreplace')[:64])
        return r

    def update_crash_info(self):
        data = list()
        url = self.base_url + '/api/crash/update'
        for crash in self.crashes:
            if crash.get('cov_stats') is None:
                continue
            new_additional = "{additional}\n{cov_stats}\n".format(**crash)
            form = {'stat_id': crash['stat_id'],
                    'crash_hash': crash['crash_hash'],
                    'bb_count': crash['cov_stats']['bb_mu'],
                    'bb_counts': crash['cov_stats'],
                    'additional': new_additional}
            if 'short_description' not in crash:
                data.append(form)
                continue
            elif 'GracefulExit' in crash['short_description']:
                form['verified'] = -1
            elif crash['classification'] in exploitable_levels:
                form['verified'] = exploitable_levels[crash['classification']]
            else:
                form['verified'] = 2
            if 'bug_id' in crash:
                form['bug_id'] = crash['bug_id']
            form['exploitability'] = crash['classification']
            form['stack_hash'] = crash['hash']
            form['additional'] += "\n\t{}\n".format(crash['short_description'])
            form['additional'] += "{}\n".format(crash['instruction'])
            form['additional'] += "{}\n".format("\n".join(crash['stack_frame']))
            data.append(form)
        r = requests.post(url, headers=self.auth_header, json=data)
        j = safe_json(r.text)
        logger.info(j['message'])
        return j.get('updated')

    def get_crashes(self, limit=10):
        """
        Download a list of crash info records to be verified from LuckyFuzz server.
          (the actual crash sample is downloaded in process_crashes).
        """

        local_dir = os.path.join(self.base_dir, self.install_dir, self.prefix)
        target = os.path.join(local_dir, self.binary_path.replace('built/', ''))
        if not os.path.isfile(target):
            print("ERROR: target file not found: {}".format(target))
            return []
        url = self.base_url + '/api/crash/verify'
#       target_hash = sha1sum(target)
#       form = {'target_hash': target_hash, 'limit': limit}
        form = {'challenge_id': self.challenge_id, 'limit': limit}
        if self.filter_options.get('null_bug_id'):
            form['bug_id'] = "1"
        if self.filter_options.get('null_coverage'):
            form['coverage'] = "1"
        if self.filter_options.get('verified'):
            form['verified'] = self.filter_options['verified']
        r = requests.post(url, headers=self.auth_header, data=form)
        j = safe_json(r.text)
        if not j['success']:
            logger.error(j['message'], form)
            return []
        crashes = j['data']
        return crashes

    def process_crashes(self, limit=10, execute_native=False):
        """
        Process the list of crashes:
          - download the sample input
          - run the GDB exploitable plugin
          - run the program with Dynamorio drrun/drcov
          - run the program to capture LAVALOG bug id
        """

        self.crashes = self.get_crashes(limit)
        if len(self.crashes) == 0:
            print("[-] No crashes available to verify.")
            return 0
        logger.info("Executing: %s", self.get_target_command('.current_input', lavalog=True))
        for crash in self.crashes:
            url = self.base_url + '/api/crash/download/{}'.format(crash['crash_id'])
            r = requests.get(url, headers=self.auth_header)
            with open('.current_input', 'wb') as f:
                f.write(r.content)
            if self.run_exploitable(crash) is None:
                continue
            logger.info("crash_id: %(crash_id)s\t: %(classification)s\t %(short_description)s", crash)
            save_logs = 'GracefulExit' not in crash.get('short_description')
            crash['cov_stats'] = self.run_with_dynamorio(crash, save_logs=save_logs)
            if self.source_dir and execute_native:
                crash.update(self.run_binary('.current_input', verbose=False))
        return self.update_crash_info()

    def run_with_dynamorio_custom(self, input_file, custom_client):
        """
        Run the program with Dynamorio using custom client
        """

        tgt_cmd = self.get_target_command(input_file, lavalog=True)
        tgt_cmd = shlex.split(tgt_cmd)

        is64 = is64bit(tgt_cmd[0])
        drrun = DRRUN if is64 else DRRUN32
        stdin_fd = open(input_file, 'rb') if self.use_stdin else DEVNULL
        with tempfile.TemporaryDirectory() as out:
            args = [drrun, '-c', custom_client, '-log_dir', out, '--']+tgt_cmd
            logger.info("%s", " ".join(args))
            try:
                run(args, stdin=stdin_fd, stdout=DEVNULL, stderr=DEVNULL, timeout=30)
            except TimeoutExpired:
                logger.error("Timeout in drrun")
            files = glob.glob("{}/*.log".format(out))
            try:
                stats = json.load(open(files[0])) if files else None
            except ValueError:
                print("ERROR in {}".format(open(files[0]).read()))

            if stats is None:
                logger.error("ERROR: failed to loading log file")
                return None
            stats['challenge'] = self.install_dir
            stats['chall_id'] = self.challenge['challenge_id']
            stats['input'] = input_file

        if self.use_stdin:
            stdin_fd.close()

        return stats
