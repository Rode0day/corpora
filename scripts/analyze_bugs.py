#!/usr/bin/env python3

import argparse
import json
import os
import re
import sqlite3
import subprocess
import sys
import yaml


p_lava_set = re.compile('lava_set\(([0-9]+)')  # noqa: W605
p_lava_get = re.compile("lava_get\(([0-9]+)")  # noqa: W605
p_lava_log = re.compile(".*LAVALOG\(([0-9]+),.*")  # noqa: W605
p_lava_bug = re.compile(".*LAVABUG\(([0-9]+)\).*")  # noqa: W605
p_data_flow = re.compile("data_flow\[([0-9]+)")  # noqa: W605
p_lava_dfl = re.compile("DFLOG\(([0-9]+)")  # noqa: W605


def parse_info(yaml_file):
    """
    Parse info.yaml for the current competition, return parsed yaml object
    """
    if not os.path.isfile(yaml_file):
        raise RuntimeError("Missing info.yaml file: {}".format(yaml_file))

    info = yaml.load(open(yaml_file), Loader=yaml.SafeLoader)
    return info


class LavaParser(object):

    def __init__(self, base_dir, info, compact=False):
        self.type = ('dflog', 'data_flow')
        self.compact = compact
        self.sets = dict()  # dictionary of lava_set/DFLOG indexed by id
        self.gets = dict()  # dictionary of lava_get, currently unused
        self.bugs = dict()  # dictionary of all bugs, indexed by bug_id
        self.locs = dict()  # dictionary of address lists indexed by (src_line, line_no)
        self.base_dir = base_dir
        self.target = info['install_dir']
        self.binary = info['binary_path']
        self.chall_id = info['challenge_id']
        self.db_con = None

        self.check_lava_set()  # parse all lava_set/DFLOG
        self.check_lava_log()  # parse all LAVALOG/LAVABUG

    def check_lava_set(self):
        """
        grep for either lava_set r DFLOG in all source files
        """
        args = "grep -n 'lava_set([0-9]' {}/src/*/*.c".format(self.base_dir)
        p = subprocess.run(args, stdout=subprocess.PIPE, shell=True, universal_newlines=True)

        if p.returncode == 0:
            self.type = ('lava_set', 'lava_get')
            self.parse_lava_set(p.stdout, p_lava_set)
            return

        args = "grep -n 'DFLOG([0-9]' {}/src/*/*.c".format(self.base_dir)
        p = subprocess.run(args, stdout=subprocess.PIPE, shell=True, universal_newlines=True)

        if p.returncode == 0:
            self.type = ('dflog', 'data_flow')
            self.parse_lava_set(p.stdout, p_lava_dfl)

    def parse_lava_set(self, output, re_obj):
        """
        Parse the output from check_lava_set, should be one per line
        """
        for line in output.splitlines():
            if ':' not in line:
                print("MISSING ':' {}".format(line))
                continue
            src_path, line_no, source = line.split(':', maxsplit=2)
            source = source.strip()
            src_file = os.path.basename(src_path)
            m = re_obj.findall(source)
            s = list(set(m))

            self.sets[s[0]] = {
                'src_path': src_path,
                'src_file': src_file,
                'line_no': line_no,
                'source': source,
                'sets': s
            }

    def check_lava_log(self):
        """
        grep for LAVALOG/LAVABUG in all source files
        """
        args = "grep -n 'LAVALOG([0-9]' {}/src/*/*.c".format(self.base_dir)
        p = subprocess.run(args, stdout=subprocess.PIPE, shell=True, universal_newlines=True)

        if p.returncode == 0:
            self.parse_lava_log(p.stdout, p_lava_log)
            return True

        args = "grep -n 'LAVABUG([0-9]' {}/src/*/*.c".format(self.base_dir)
        p = subprocess.run(args, stdout=subprocess.PIPE, shell=True, universal_newlines=True)

        if p.returncode == 0:
            self.type = ('none', 'covbug')
            self.parse_lava_log(p.stdout, p_lava_bug)

    def parse_lava_log(self, output, re_obj):
        """
        parse the output from check_lava_log, should be one per line,
          but this is not true.  There are a low number where the
          LAVALOG is split across multiple lines and other cases where
          two LAVALOGs are nested.  Currently, we add the LAVALOG, but
          ignore the gets/sets when the LAVALOG is not all on one line.
        """
        for line in output.splitlines():
            if ':' not in line:
                print("MISSING ':' {}".format(line))
                continue
            src_path, line_no, source = line.split(':', maxsplit=2)
            source = source.strip()
            src_file = os.path.basename(src_path)
            m_log = re_obj.match(source)
            if m_log:
                bug_id = m_log.group(1)
            else:
                print("Missing? {}".format(source))
                continue
            self.bugs[bug_id] = {
                'src_path': src_path,
                'src_file': src_file,
                'line_no': line_no,
                'source': source,
                'bug_id': bug_id,
            }
            if 'LAVALOG' in source:
                self.lookup_lava_sets(bug_id, source)
            else:
                self.bugs[bug_id]['sets'] = {}
                self.bugs[bug_id]['gets'] = {}

    def lookup_lava_sets(self, bug_id, source):
        """
        Find the matching id(s) in the parsed lava_set/dflog
        """
        bug_sets = dict()
        m = []
        if 'lava_get' in source:
            m = p_lava_get.findall(source)
            if len(m) == 0:
                print("MISSING lava_get {}".format(source))
        elif 'data_flow' in source:
            m = p_data_flow.findall(source)
            if len(m) == 0:
                print("MISSING data_flow {}".format(source))
        else:
            print("MISSING lava_get/data_flow:  {}".format(source))

        s = list(set(m))
        self.bugs[bug_id]['gets'] = s
        for s0 in s:
            if s0 in self.sets:
                bug_sets[s0] = self.sets.get(s0)
            else:
                print(f"MISSING lava_set/dflog for id:{s0}")
        self.bugs[bug_id]['sets'] = bug_sets

    def write_stats(self, compact=False):
        """
        Write the bug data to a json file in the install_dir
        """
        challenge = {self.base_dir: {
            'bugs': self.bugs,
            'gets': self.gets,
            'sets': self.sets,
        }}

        with open(f"{self.base_dir}/bugs.json", 'w') as fd:
            if self.compact or compact:
                json.dump(challenge, fd, sort_keys=True)
            else:
                json.dump(challenge, fd, indent=4, sort_keys=True)

    def summary_stats(self):
        """
        Print a summmary stats line
        """
        n_bugs = len(self.bugs)
        bug_type = self.type[1]
        d1 = sum([1 for b in self.bugs.values() if len(b['sets']) == 1])
        d3 = sum([1 for b in self.bugs.values() if len(b['sets']) == 3])
        print(f"Summary: {self.chall_id:3}-{self.target:20} {n_bugs:4} bugs, {d1:3} 1-dua, {d3:3} 3-dua, type={bug_type}")  # noqa

    def iter_bugs(self):
        """
        Helper for storing in database, yields all bugs as tuples
        """
        for k, b in self.bugs.items():
            yield (
                b['bug_id'],
                b['src_file'],
                b['line_no'],
                b['source'],
                len(b['gets']),
                len(b['sets']),
                self.target,
                self.chall_id,
                self.type[1]
            )

    def iter_sets(self):
        """
        Helper for storing in database, yields sets as tuples
        """
        for k, b in self.bugs.items():
            for s in b['sets'].values():
                if s is None:
                    continue
                yield (
                    b['bug_id'],
                    s['src_file'],
                    s['line_no'],
                    s['source'],
                    self.chall_id,
                    self.type[0]
                )

    def init_db(self, db_filename=":memory:"):
        """
        Initialize the database tables and connection
        """
        self.db_con = sqlite3.connect(db_filename)
        q_create_bugs = ("CREATE TABLE data(id INTEGER PRIMARY KEY, bug_id INTEGER, "
                         "src_file TEXT, line_no INTEGER, "
                         "target TEXT, chall_id INTEGER, type TEXT, "
                         "n_gets INTEGER, n_sets INTEGER, source TEXT)")
        q_create_addr = ("CREATE TABLE block_map(id INTEGER PRIMARY KEY, bug_id INTEGER, "
                         "data_id INTEGER, addr INTEGER)")
        with self.db_con:
            self.db_con.execute(q_create_bugs)
            self.db_con.execute(q_create_addr)

    def save_to_db(self, db_filename=":memory:"):
        """
        Save the results to the database, no uniqueness is enforced, so processing
          the same binary would result in duplicate entries
        """
        if self.db_con is None and os.path.isfile(db_filename):
            self.db_con = sqlite3.connect(db_filename)
        else:
            self.init_db(db_filename)

        with self.db_con:
            q_str = ("INSERT INTO data(bug_id, src_file, line_no, source, n_gets, n_sets, "
                     "target, chall_id, type) "
                     "VALUES (?, ?, ?, ? ,?, ?, ?, ?, ?)")
            self.db_con.executemany(q_str, self.iter_bugs())

            q_str = ("INSERT INTO data(bug_id, src_file, line_no, source, chall_id, type) "
                     "VALUES (?, ?, ?, ?, ?, ?)")
            self.db_con.executemany(q_str, self.iter_sets())

        with self.db_con:
            self.insert_blocks()

    def dump_db(self):
        """
        Not used, but here as a helper fuction
        """
        with open('dump.sql', 'w') as f:
            for line in self.db_con.iterdump():
                f.write(f"{line}")

    def locate_blocks(self):
        """
        Uses pyelftools to find the addresses in a compiled binary with debug symbols
          corresponding to each LAVALOG and lava_set/DFLOG
        """
        if not os.path.isfile(self.binary):
            print(f"Error: binary file {self.binary} not found!")
        for b in self.bugs.values():
            self.locs[(b['src_file'], int(b['line_no']))] = {'bug_id': b['bug_id'], 'addrs': list()}
            for s in b['sets'].values():
                if s is None:
                    continue
                self.locs[(s['src_file'], int(s['line_no']))] = {'bug_id': b['bug_id'], 'addrs': list()}
        import dwarf_lineprogram_filenames as dlf
        dlf.process_file(self.binary, self.locs)

    def insert_blocks(self):
        """
        Insert the matched adddresses into the database.  The relationship is data.id == block_map.data_id.
        """
        for k, b in self.locs.items():
            r = self.db_con.execute("SELECT id FROM data WHERE bug_id = ? AND src_file = ? and LINE_no = ?",
                                    (b['bug_id'], k[0], k[1]))
            row = r.fetchone()
            if row:
                data_id = row[0]
            else:
                print("DB: failed to find {}, {}:{}".format(b['bug_id'], k[0], k[1]))
                continue
            for a in b['addrs']:
                self.db_con.execute("INSERT INTO block_map(bug_id, data_id, addr) VALUES (?, ?, ?)",
                                    (b['bug_id'], data_id, a))


def parse_lava_get(base_dir):
    """
    Currently not used, but parses all lava_get(s) from the source files.
      There is no corresponding function for data_flow parsing, which is harder to isolate.
    """
    args = "grep -n 'lava_get([0-9]' {}/src/*/*.c".format(base_dir)
    p = subprocess.run(args, stdout=subprocess.PIPE, shell=True, universal_newlines=True)

    if p.returncode != 0:
        return False

    lava_gets = dict()
    for line in p.stdout.splitlines():
        if ':' not in line:
            print("MISSING ':' {}".format(line))
            continue
        src_path, line_no, source = line.split(':', maxsplit=2)
        src_file = os.path.basename(src_path)
        m = p_lava_get.findall(source)
        s = list(set(m))

        lava_gets[s[0]] = {
            'src_path': src_path,
            'src_file': src_file,
            'line_no': line_no,
            'source': source,
            'gets': s}
    return lava_gets


def process_challenge(info, args, challenge_name):
    """
    Process a single challenge given the yaml info data, commandline args
      and the unique name of the challenge.
    """
    challenge = info['challenges'][challenge_name]
    bdir = "{}/{}".format(info['rode0day_id'], challenge['install_dir'])
    challenge['prefix'] = args.prefix
    challenge['binary_path'] = os.path.join(bdir, info['prefix'], challenge['binary_path'].replace('built/', ''))

    LP = LavaParser(bdir, challenge)
    if not args.no_binary:
        LP.locate_blocks()
    LP.summary_stats()
    if args.json_file:
        LP.write_stats(args.compact)
    if args.database:
        LP.save_to_db(args.database)


def main():
    parser = argparse.ArgumentParser(description='test Rode0day binaries on inputs')
    parser.add_argument('-c', '--challenge', default=None,
                        help='process a single challenge name/ install_dir')
    parser.add_argument('-p', '--prefix', default='lava-install',
                        help='install directory prefix')
    parser.add_argument('-d', '--database', default=None,
                        help='save data to sqlite3 database file')
    parser.add_argument('-j', '--json-file', default=False, action='store_true',
                        help='save data to json file in target directory <bugs.json>')
    parser.add_argument('--compact', default=False, action='store_true',
                        help='store json in compact format instead of indented')
    parser.add_argument('-n', '--no-binary', default=False, action='store_true',
                        help=('Do NOT process binary for address locations.'
                              ' (requires debug symbols)'))
    parser.add_argument('info_yaml', nargs='+', help='file path(s) for info.yaml file')
    args = parser.parse_args()

    for yaml_file in args.info_yaml:
        info = parse_info(yaml_file)
        info['prefix'] = args.prefix
        if args.challenge:
            process_challenge(info, args, args.challenge)
            return

        for chall in info['challenges'].keys():
            process_challenge(info, args, chall)


if __name__ == '__main__':
    main()
    sys.exit(0)
