#!/bin/bash

SRC_DIRS="12/grepB2/src/src
2/yamlB2/src/src
2/yamlB2/src/libyaml-dist-0.2.3
4/pcreB/src/src
4/pcreB/src/pcre2-10.34
5/duktape/src/src
5/duktape/src/duktape-2.3.0/src
6/jqB/src/src
6/jqB/src/jq-1.6
7/fileB3/src/src
7/fileB3/src/file-FILE5_38
9/sqliteB/src/src
9/sqliteB/src/sqlite-autoconf-3290000
10/tcpdumpB/src/src
10/tcpdumpB/src/libpcap
10/tcpdumpB/src/tcpdump-tcpdump-4.9.3
10/tcpdumpB/src/libpcap-libpcap-1.9.1
11/jpegS3/src/src
11/jpegS3/src/jpeg-9c
11/tinyexprB2/src/src
11/tinyexprB2/src/tinyexpr-master
12/grepB2/src/src
12/grepB2/src/grep-3.4"


for d in $SRC_DIRS; do
	printf "[*] === %40s === \t" ${d}
	gocloc --skip-duplicated --include-lang="C,C Header"  "$d" | grep TOTAL | LC_ALL=en_US.UTF-8 awk  "{ printf \"%'d\n\",  \$5 }"
done
