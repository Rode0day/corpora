#!/usr/bin/env python3

import argparse
import re
import subprocess
import sys

import pandas as pd
import pysqlite3 as sqlite3

prefixes = {
  'duktape': ('5', '/duktape/src/', 'duk'),
  'fileB3': ('7', '/fileB3/src/', 'file'),
  'fileS3': ('12', '/fileS3/src/', 'file'),
  'fileS4': ('13', '/fileS4/src/', 'file'),
  'grepB2': ('12', '/grepB2/src/', 'grep'),
  'jpegS3': ('11', '/jpegS3/src/', 'jpeg'),
  'jqB': ('6', '/jqB/src/', 'jq'),
  'jqB2': ('8', '/jqB2/src/', 'jq'),
  'jqS3': ('11', '/jqS3/src/', 'jq'),
  'jqS4': ('12', '/jqS4/src/', 'jq'),
  'newgrepS': ('4', '/newgrepS/src/', 'newgrep'),
  'pcreB': ('4', '/pcreB/src/', 'pcre2grep'),
  'sqliteB': ('9', '/sqliteB/src/', 'sqlite'),
  'tcpdumpB': ('10', '/tcpdumpB/src/', 'tcpdump'),
  'tinyexprB2': ('11', '/tinyexprB2/src/', 'math'),
  'yamlB2': ('2', '/yamlB2/src/', 'deconstruct'),
  'base64': ('0', '/lava_corpus/LAVA-M/base64/coreutils-8.24-lava-safe/', 'base64'),
  'md5sum': ('0', '/lava_corpus/LAVA-M/md5sum/coreutils-8.24-lava-safe/', 'md5sum'),
  'uniq':   ('0', '/lava_corpus/LAVA-M/uniq/coreutils-8.24-lava-safe/', 'uniq'),
  'who':    ('0', '/lava_corpus/LAVA-M/who/coreutils-8.24-lava-safe/', 'who'),
}


def run_addr2line(obj_file, addresses):
    args = ['addr2line', '-e', obj_file, '-afp']
    p = subprocess.run(args=args, input=addresses, stdout=subprocess.PIPE, universal_newlines=True)
    return p.stdout


def parse_result(lines, prefix, logfile):
    csvfile = open(logfile, 'w')
    results = list()
    prog = re.compile('(0x[a-f0-9]+): (\S+)(?: at | )?(\S+):(\S+)(?: .discriminator (\d*).*)?')  # noqa
    for line in lines.split('\n'):
        if line[:10] == '0xffffffff' or len(line) < 12:
            continue
        line = line.replace(f"/builds/Rode0day/corpora/{prefix}", '')
        r = prog.match(line)
        if r:
            g = r.groups(default='0')
            addr_b10 = int(r[1], 16)
            offset = addr_b10 - 0x8048000
            csvfile.write(','.join(g))
            csvfile.write(",{},{}\n".format(addr_b10, offset))
            results.append((addr_b10, r[2], r[3], r[4], r[5]))
        else:
            sys.stderr.write(f"[-] Error in regex for :{line}\n")
    csvfile.close()
    return results


def get_blocks(db_filepath, target):
    conn = sqlite3.connect(db_filepath)
    sql = "SELECT id FROM target WHERE name = ? AND arch = 32"
    r = conn.execute(sql, (target,))
    target_id = r[0]

    sql = "SELECT id,address FROM block WHERE target_id = {target_id}"
    df_b = pd.read_sql_query(sql, conn)


def process_csvfile(args):
    df = pd.read_csv(args.csv)
    targets = df.target_name.unique()
    df['hex_addr'] = df.address.apply(hex)

    for target in targets:
        df_tgt = df[df.target_name == target]
        prefix = prefixes[target]
        build_prefix = ''.join(prefix[:2])
        exe = f"{prefix[0]}/{target}/lava-gcc/bin/{prefix[2]}"
        if args.write_blocks:
            df_tgt.hex_addr.to_csv(f"{target}_blocks.csv", header=False, index=False)
        addresses = '\n'.join(df_tgt.hex_addr)
        src_lines = run_addr2line(exe, addresses)
        parsed = parse_result(src_lines, prefix=build_prefix, logfile=f"{target}_covered_lines.csv")


def main():
    parser = argparse.ArgumentParser(description="Extract source coverage from block adddresses")
    parser.add_argument('csv', help='monitor_block_list.csv.gz file')
    parser.add_argument('--write-blocks', action='store_true', default=False,
                        help='write a blocks.csv file per target')
    args = parser.parse_args()
    process_csvfile(args)


if __name__ == '__main__':
    main()
